# Automação API
Projeto de automação de testes para as APIs

## Instalação
Clonar o projeto pela URL: https://gitlab.com/djonathan_kaiper/fipe_api.git


## Como rodar

Para rodar os cenários do projeto:
- Executar o "bundle install"
- Executar o comando do cenário "cucumber -t @Fipe_Api"
