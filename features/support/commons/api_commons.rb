class ApiCommons

  def busca_id_marca(response, marca)
    response.each do |marcas|
      if marcas['nome'].eql?(marca)
        @id_marca = marcas['codigo']
      end
    end
    @id_marca
  end

  def busca_id_modelo(response, id_marca, modelo)
    response['modelos'].each do |modelos|
      if modelos['nome'].eql?(modelo)
        @id_modelo = modelos['codigo']
      end
    end
    @id_modelo
  end

  def busca_id_ano(response, id_marca, id_modelo, ano)
    response.each do |anos|
      if anos['nome'].include?(ano)
        @id_ano = anos['codigo']
      end
    end
    @id_ano
  end

end
