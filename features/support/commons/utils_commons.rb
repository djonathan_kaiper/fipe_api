class UtilsCommons

  def HTTParty(tipo, url_marca)
    response = HTTParty.get(url_marca) if tipo.eql?('get')
    response = HTTParty.post(url_marca) if tipo.eql?('post')
    response
  end

  def show_object(obj, status)
    "JSON status[#{status}]:\n#{JSON.pretty_generate(JSON.parse("#{obj}"))}"
  end

  def json_validate(response, status)
    validation_schema = eval(File.read("features/schemas/#{status}.schema"))
    JSON::Validator.validate!(validation_schema, response.to_s)
  end
end
