# language:pt


@Fipe_Api
Funcionalidade: Fipe_Api - Validar o endpoint Fipe_Api.get
  Como uma aplicação de APIs
  Quero chamar o endpoint da API
  Para validar a funcionalidade do mesmo

  Esquema do Cenário: Validar o valor do veiculo e codigo da FIPE
    Quando consultar os dados do veiculo no endpoint Fipe_Api.get
      | veiculo | <veiculo> |
      | tipo    | <tipo>    |
      | ano     | <ano>     |
      | marca   | <marca>   |
      | modelo  | <modelo>  |
    Então validar que o valor do veiculo é 'R$ 58.739,00' e o codigo da fipe '004381-8'

  Exemplos:
    | veiculo | tipo   | ano  | marca          | modelo                              |
    | carros  | marcas | 2016 | GM - Chevrolet | CRUZE LTZ 1.8 16V FlexPower 4p Aut. |