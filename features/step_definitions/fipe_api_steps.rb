Quando("consultar os dados do veiculo no endpoint Fipe_Api.get") do |dados|
  # Buscar o ID da marca
  url_marca = "https://parallelum.com.br/fipe/api/v1/#{dados.rows_hash['veiculo']}/#{dados.rows_hash['tipo']}"
  response = @UtilsCommons.HTTParty('get', url_marca)
  @UtilsCommons.json_validate(response, 200)
  id_marca = @ApiCommons.busca_id_marca(response, dados.rows_hash['marca'])

  # Buscar o ID do modelo
  url_modelos = "https://parallelum.com.br/fipe/api/v1/carros/marcas/#{id_marca}/modelos"
  response = @UtilsCommons.HTTParty('get', url_modelos)
  @UtilsCommons.json_validate(response, '200_modelo')
  id_modelo = @ApiCommons.busca_id_modelo(response, @id_marca, dados.rows_hash['modelo'])

  # Buscar o ID do ano
  url_anos = "https://parallelum.com.br/fipe/api/v1/carros/marcas/#{id_marca}/modelos/#{id_modelo}/anos"
  response = @UtilsCommons.HTTParty('get', url_anos)
  @UtilsCommons.json_validate(response, 200)
  id_ano = @ApiCommons.busca_id_ano(response, id_marca, id_modelo, dados.rows_hash['ano'])

  # Buscar os dados do veiculo
  url_veiculo = "https://parallelum.com.br/fipe/api/v1/carros/marcas/#{id_marca}/modelos/#{id_modelo}/anos/#{id_ano}"
  @consulta_veiculo = HTTParty.get(url_veiculo)
  @UtilsCommons.json_validate(@consulta_veiculo, '200_veiculo')
end

Então("validar que o valor do veiculo é {string} e o codigo da fipe {string}") do |valor, id_fipe|
  # Validação veiculo
  expect(@consulta_veiculo.response.code.to_i).to eql(200)
  expect(@consulta_veiculo['Valor']).to eql(valor)
  expect(@consulta_veiculo['CodigoFipe']).to eql(id_fipe)

  puts @UtilsCommons.show_object(@consulta_veiculo, 200)
end
